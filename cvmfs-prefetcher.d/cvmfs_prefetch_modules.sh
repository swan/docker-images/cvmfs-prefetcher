#!/bin/bash

while true; do
    source /cvmfs/sft.cern.ch/lcg/views/LCG_99/x86_64-centos7-gcc8-opt/setup.sh && ( timeout 10s python -m ipykernel > /dev/null 2>&1 || true )
    if [ $? -ne 0 ]; then
        echo "Getting ipykernel failed for LCG_99"
    fi

    source /cvmfs/sft.cern.ch/lcg/views/LCG_99python2/x86_64-centos7-gcc8-opt/setup.sh && ( timeout 10s python -m ipykernel > /dev/null 2>&1 || true )
    if [ $? -ne 0 ]; then
        echo "Getting ipykernel failed for LCG_99python2"
    fi

    source /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/setup.sh && ( timeout 10s python -m ipykernel > /dev/null 2>&1 || true )
    if [ $? -ne 0 ]; then
        echo "Getting ipykernel failed for LCG_95apython3_nxcals"
    fi

    source /cvmfs/sft.cern.ch/lcg/views/LCG_99cuda/x86_64-centos7-gcc8-opt/setup.sh && ( timeout 10s python -m ipykernel > /dev/null 2>&1 || true )
    if [ $? -ne 0 ]; then
        echo "Getting ipykernel failed for LCG_99cuda"
    fi

    sleep 15m
done

exit 1
