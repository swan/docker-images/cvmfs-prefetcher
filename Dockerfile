FROM cern/cc7-base:20181210

MAINTAINER SWAN Admins <swan-admins@cern.ch>

# ----- Set environment and language ----- #
ENV DEBIAN_FRONTEND noninteractive
ENV LANGUAGE en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

# ----- Remove Unwanted Packages ----- #
RUN yum -y remove \
      yum-autoupdate

# ----- Generic Tools ----- #
RUN yum -y install \
      which && \
    yum clean all && \
    rm -rf /var/cache/yum

ADD ./cvmfs-prefetcher.d/cvmfs_prefetch_modules.sh /etc/cvmfs-prefetcher/cvmfs_prefetch_modules.sh
CMD ["/bin/bash", "/etc/cvmfs-prefetcher/cvmfs_prefetch_modules.sh"]
