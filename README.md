### Docker image for cvmfs prefetcher

This is repository with the docker image holding all the dependencies required for prefetching CVMFS repositories on the hosts.

#### Entrypoint 

User of the image should overwrite the default entrypoint at path below in order to customize the prefething logic

```
/etc/cvmfs-prefetcher/cvmfs_prefetch_modules.sh
```